package com.nexura.calireporta.adapters;


/**
 * Created by ervic on 12/11/17.
 */


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nexura.calireporta.R;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.activities.SingleViewActivity;
import com.nexura.calireporta.models.EventoSingle;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by ervic on 11/11/17.
 */

public class RecentCasesAdapter extends RecyclerView.Adapter<RecentCasesAdapter.CardViewrecentEvent> {
    private Context activity;
    private List<EventoSingle> list_myevents;
    private int COMENTARIO_OK = 222;

    public RecentCasesAdapter (Context context, List<EventoSingle> misEventos) {
        this.activity = context;
        this.list_myevents = misEventos;
    }

    @Override
    public CardViewrecentEvent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recents_events,parent,false);
        return new CardViewrecentEvent(v);
    }
    @Override
    public void onBindViewHolder(final CardViewrecentEvent holder, final int position) {
        final EventoSingle eventoSingle = list_myevents.get(position);
        holder.itemView.setTag(eventoSingle);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs sharedPrefs = new SharedPrefs(activity);
                if(!sharedPrefs.readSharedSetting("userId","").contains(eventoSingle.usuario_id)){
                    View view = LayoutInflater.from(activity).inflate(R.layout.layout_detail_alert, null);
                    TextView titulo = view.findViewById(R.id.titulo);
                    TextView categoria = view.findViewById(R.id.categoria);
                    TextView estado = view.findViewById(R.id.estado);
                    TextView fecha = view.findViewById(R.id.fecha);
                    titulo.setText(eventoSingle.titulo);
                    categoria.setText(eventoSingle.categoria.nombre);
                    estado.setText(eventoSingle.estado.nombre);
                    fecha.setText(eventoSingle.fecha);
                    Utils.showDialog(activity,"Detalle",view);
                    Log.e("USER SHARED", sharedPrefs.readSharedSetting("userId",""));
                    Log.e("USER EVENT", eventoSingle.usuario_id);
                } else {
                    Intent intent = new Intent(activity, SingleViewActivity.class);
                    intent.putExtra("id", eventoSingle.id);
                    intent.putExtra("from", 0);
                    ((Activity) activity).startActivityForResult(intent, COMENTARIO_OK);
                }
            }
        });
        holder.titulo.setText(eventoSingle.titulo);
        holder.lugar.setText(eventoSingle.dir);
        holder.estado.setText("Estado: " + eventoSingle.estado.nombre);
        try
        {
            if(!eventoSingle.archivos.get(0).isEmpty()){
                String server = activity.getResources().getString(R.string.server);
                server=server+eventoSingle.archivos.get(0);
                Picasso.with(activity)
                      .load(server)
                      .placeholder(R.drawable.sin_imagen)
                      .centerCrop()
                      .error(R.drawable.sin_imagen)
                      .into(holder.imagen);
            } else {
                holder.imagen.setImageDrawable(null);
            }

            

        }
        catch (Exception e)
        {
            Log.d("MyGov",e.toString());
        }
    }

    @Override
    public int getItemCount() {
        if(list_myevents == null)
            return 0;
        return list_myevents.size();
    }


    public class CardViewrecentEvent extends RecyclerView.ViewHolder{
        protected TextView titulo;
        protected TextView estado;
        protected ImageView imagen;
        protected TextView lugar;
        protected TextView id;

        public CardViewrecentEvent(View itemView) {
            super(itemView);

            titulo = (TextView) itemView.findViewById(R.id.titulo_recent);
            estado = (TextView) itemView.findViewById(R.id.estado_recent);
            imagen = (ImageView) itemView.findViewById(R.id.img_recent);
            lugar = (TextView) itemView.findViewById(R.id.lugar_recent);
            id = (TextView) itemView.findViewById(R.id.id_recent);
        }
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
