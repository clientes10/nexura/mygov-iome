package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 07/04/2017.
 */

public class ResponseEventosAfecta {
    public List<EventoSingle> eveAfecta;
    public boolean success;
}
