package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by ervic on 7/3/18.
 */

public class ResponseValoraciones {

    public boolean success;
    public List<Valoraciones> valoraciones;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Valoraciones> getValoraciones() {
        return valoraciones;
    }

    public void setValoraciones(List<Valoraciones> valoraciones) {
        this.valoraciones = valoraciones;
    }
}
