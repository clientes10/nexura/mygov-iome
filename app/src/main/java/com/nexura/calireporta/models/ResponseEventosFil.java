package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 17/05/17.
 */

public class ResponseEventosFil {
    public List<EventoSingle> eventosFil;
    public boolean success;
}
