package com.nexura.calireporta.models.ui;

/**
 * Created by javpoblano on 16/04/2017.
 */

public class LocalidadSpinnerItem {
    public String name;
    public int id;

    public LocalidadSpinnerItem(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    //Spinner methods

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof LocalidadSpinnerItem){
            LocalidadSpinnerItem c = (LocalidadSpinnerItem)obj;
            if(c.getName().equals(name) && c.getId()==id ) return true;
        }
        return false;
    }
}
