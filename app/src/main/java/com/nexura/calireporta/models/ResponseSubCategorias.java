package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 27/03/17.
 */

public class ResponseSubCategorias {
    public List<SubCategoria> subcategorias;
    public boolean success;
}
