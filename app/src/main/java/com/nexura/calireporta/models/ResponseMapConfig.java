package com.nexura.calireporta.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseMapConfig {
   @SerializedName("data")
   @Expose
   public MapConfig data;

}
