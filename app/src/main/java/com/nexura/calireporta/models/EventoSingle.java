package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 28/03/17.
 */

public class EventoSingle {
    public int id;
    public String titulo;
    public String descripcion;
    public Localidad localidad;
    public CategoriaSingle categoria;
    public String fecha,fechaP,fechaF,x,y,dir;
    public Estado estado;
    public List<Comentario> comentarios;
    public int me_afecta_cuenta;
    public List<String> archivos;
    public int mi_valoracion;
    public String usuario_id;
    public String ico;
}
