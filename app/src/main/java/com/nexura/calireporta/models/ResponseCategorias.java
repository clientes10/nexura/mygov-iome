package com.nexura.calireporta.models;

import java.util.List;

/**
 * Created by javpoblano on 19/03/17.
 */

public class ResponseCategorias {
    public List<Categoria> categorias;
    public boolean success;
}
