package com.nexura.calireporta.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by javpoblano on 26/03/17.
 */

public class ResponseEventos{
    public List<EventoSingle> eventos;
    public boolean success;


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("success", success).append("eventos", eventos).toString();
    }


}
