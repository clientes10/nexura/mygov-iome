package com.nexura.calireporta.models;

/**
 * Created by javpoblano on 20/03/17.
 */

public class Estado {
    public int id;
    public String nombre;

    public Estado(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Estado){
            Estado c = (Estado)obj;
            if(c.getNombre().equals(nombre) && c.getId()==id ) return true;
        }

        return false;
    }
}
