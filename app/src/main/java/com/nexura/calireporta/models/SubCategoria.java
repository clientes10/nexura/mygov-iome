package com.nexura.calireporta.models;

/**
 * Created by javpoblano on 27/03/17.
 */

public class SubCategoria {
    public int id;
    public int categoria_id;
    public String nombre;
    public String descripcion;
    public String cliente;
    public String estado;

    @Override
    public String toString() {
        return this.nombre;            // What to display in the Spinner list.
    }
}
