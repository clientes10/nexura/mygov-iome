package com.nexura.calireporta.models;

public class ResponseUsuario {
   
   public ResponseUsuarioData data;
   
   public class ResponseUsuarioData {
      public Usuario usuario;
   }
   public class Usuario {
      public String id;
      public String nombre;
      public String email;
   }
}
