package com.nexura.calireporta.api;


import com.nexura.calireporta.models.ResponseAllowZone;
import com.nexura.calireporta.models.ResponseCategorias2;
import com.nexura.calireporta.models.ResponseComentarios;
import com.nexura.calireporta.models.ResponseComunas2;
import com.nexura.calireporta.models.ResponseEstados2;
import com.nexura.calireporta.models.ResponseEvento2;
import com.nexura.calireporta.models.ResponseEventos;
import com.nexura.calireporta.models.ResponseEventos2;
import com.nexura.calireporta.models.ResponseEventosAfecta2;
import com.nexura.calireporta.models.ResponseImage;
import com.nexura.calireporta.models.ResponseLastUpdate;
import com.nexura.calireporta.models.ResponseLocalidades;
import com.nexura.calireporta.models.ResponseMapConfig;
import com.nexura.calireporta.models.ResponseNuevoEvento;
import com.nexura.calireporta.models.ResponseNuevoEvento2;
import com.nexura.calireporta.models.ResponseSubCategorias2;
import com.nexura.calireporta.models.ResponseTipoValoraciones;
import com.nexura.calireporta.models.ResponseTotalComentarios;
import com.nexura.calireporta.models.ResponseTotalValoraciones;
import com.nexura.calireporta.models.ResponseUsuario;
import com.nexura.calireporta.models.ResponseValoraciones;
import com.nexura.calireporta.models.ResponsecomentarEvento2;
import com.nexura.calireporta.models.ResponsenewPassword;
import com.nexura.calireporta.models.ResponsenuevoUsuario;
import com.nexura.calireporta.models.ResponsenuevoUsuario2;
import com.nexura.calireporta.models.ResponserecuperarPassword;
import com.nexura.calireporta.models.ResponseuploadEventFile;
import com.nexura.calireporta.models.ResponsevalorarEvento2;
import com.nexura.calireporta.models.User;
import com.nexura.calireporta.models.User2;
import com.nexura.calireporta.models.responsearchivos.ResponseArchivos;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by javpoblano on 19/03/17.
 */

public interface MyGovServices {
    //Nuevas urls
    @GET("api/MyGov/archivos/")
    Call<ResponseArchivos> getArchivos();

    @GET("api/MyGov/categorias")
    Call<ResponseCategorias2> getCategorias();

    @FormUrlEncoded
    @POST("api/MyGov/comentarEvento")
    Call<ResponsecomentarEvento2> comentarEvento(@Header("nxtoken") String nxtoken, @Field("evento_id") String str, @Field("comentario") String str2);

    @GET("api/MyGov/comentarios/{idevento}")
    Call<ResponseComentarios> getComentariosPorId(@Path("idevento") String id_evento);

    @GET("api/MyGov/comunas")
    Call<ResponseComunas2> getComunas();

    @GET("api/MyGov/estados")
    Call<ResponseEstados2> getEstados();

    @GET("api/MyGov/evento/{evento_id}")
    Call<ResponseEvento2> getEvento(@Path("evento_id") int id);

    @GET("api/MyGov/evento/{evento_id}")
    Call<ResponseEvento2> getEvento(@Header("nxtoken") String nxtoken,@Path("evento_id") int id);

    @GET("api/MyGov/eventos")
    Call<ResponseEventos2> getEventos();

    @GET("api/MyGov/filtraEventos")
    Call<ResponseEventos> getEventosFiltrados(@Query("categoria_id") String categoria, @Query("subcategoria_id")String subcategoria,@Query("comuna_id") String comuna,
                                            @Query("localidad_id") String localidad, @Query("estado_id") String estado,
                                            @Query("fecha_evento") String fecha);

    @GET("api/MyGov/lastUpdate")
    Call<ResponseLastUpdate> getLastUpdate();

    @GET("api/MyGov/localidades/{id_comuna}")
    Call<ResponseLocalidades> getLocalidadesPorComuna(@Path("id_comuna") int id_comuna);
    
    @GET("api/MyGov/misEventos")
    Call<ResponseEventos2> getEventosMios(@Header("nxtoken")String nxtoken);

    @POST("api/MyGov/nuevoEvento")
    @FormUrlEncoded
    Call<ResponseNuevoEvento2> createEvent(@Header("nxtoken")String nxtoken, @Field("titulo")String titulo,
                                           @Field("categoria_id")String cate, @Field("descripcion")String descr,
                                           @Field("telefono")String telefono,
                                           @Field("latitud")String lat, @Field("longitud")String lon);
    
    @GET("api/MyGov/subcategorias")
    Call<ResponseSubCategorias2> getSubCategorias();
    
    @GET("api/MyGov/tiposValoracion")
    Call<ResponseTipoValoraciones> getTipoValoraciones();

    @GET("api/MyGov/totalComentarios/{id_evento}")
    Call<ResponseTotalComentarios> getTotalComentarios(@Path("id_evento") int id_evento);

    @GET("api/MyGov/totalValoraciones/{id_evento}")
    Call<ResponseTotalValoraciones> getTotalValoracionesPorEvento(@Path("id_evento") int id_evento);
    
    @POST("api/MyGov/uploadEventFile")
    @FormUrlEncoded
    Call<ResponseImage> createImg(@Header("nxtoken")String nxtoken, @Field("evento_id")String id,
                                  @Field("evento_img")String img);

    @GET("api/MyGov/valoraciones/{id_evento}")
    Call<ResponseValoraciones> getValoracionesPorEvento(@Path("id_evento")int id_evento);

    @FormUrlEncoded
    @POST("api/MyGov/valorarEvento")
    Call<ResponsevalorarEvento2> valorarEvento(@Header("nxtoken")String nxtoken, @Field("evento_id") int i, @Field("valoracion_id") int j);

    @FormUrlEncoded
    @POST("api/MyGov/nuevoUsuario")
    Call<ResponsenuevoUsuario2> nuevoUsuario(@Field("nombre")String nombre, @Field("apellido")String apellido, @Field("correo")String email, @Field("password")String password);

    @FormUrlEncoded
    @POST("/api/MyGov/recuperaPasswrd")
    Call<ResponserecuperarPassword> recuperarPassword(@Field("email")String str);
    
    @FormUrlEncoded
    @POST("/api/Registro/newPasswd")
    Call<ResponsenewPassword> cambiarPassword(@Field("actual")String str, @Field("nueva")String str2, @Field("confirmada")String str3);
    
    @POST("loader.php?lServicio=MyGov&lTipo=apiAuth&lFuncion=login")
    @FormUrlEncoded
    Call<User> loginV(@Field("email")String email,@Field("password")String password);
    
    @POST("api/Registro/login?movil=1")
    @FormUrlEncoded
    Call<User2> login(@Field("type")String type, @Field("login")String email, @Field("password")String password);
    
    @POST("api/Registro/login?movil=1")
    @FormUrlEncoded
    Call<User2> loginFacebook(@Field("type")String type, @Field("login")String email, @Field("password")String password);
    
    @POST("/api/registro/basicSingUpFaceBook?movil=1")
    @FormUrlEncoded
    Call<User2> signinFacebook(@Field("nombre")String nombre, @Field("apellido")String apellido, @Field("email")String correo, @Field("token_facebook") String facebookId);
    
    
    @FormUrlEncoded
    @POST("api/MyGov/basicSingUpFaceBook?movil=1")
    Call<User2> nuevoUsuarioFacebook(@Field("nombre")String nombre, @Field("apellido")String apellido, @Field("email")String email, @Field("token_facebook")String token_facebook);

    //@POST("loader.php?lServicio=Participacionciudadana&lTipo=apiAuth&lFuncion=nuevoUsuario")
    //Call<ResponseNuevoUsuario> createUser(@Body NuevoUsuario user);



    @FormUrlEncoded
    @POST("loader.php?lServicio=MyGov&lTipo=apiAuth&lFuncion=nuevoEvento")
    Call<ResponseNuevoEvento> nuevoEvento(@Field("titulo") String str, @Field("categoria_id") int i, @Field("descripcion") String str2, @Field("direccion") String str3, @Field("longitud") float lat, @Field("latitud")float lng);

    @FormUrlEncoded
    @POST("loader.php?lServicio=MyGov&lTipo=apiAuth&lFuncion=uploadEventFile")
    Call<ResponseuploadEventFile> cargarImagenEvento(@Field("evento_img")String str, @Field("evento_id")int i);

    @FormUrlEncoded
    @POST("loader.php?lServicio=MyGov&lTipo=reportaApi&lFuncion=nuevoUsuario")
    Call<ResponsenuevoUsuario> nuevoUsuarioV(@Field("nombre")String nombre, @Field("apellido")String apellido, @Field("correo")String email, @Field("password")String password);
    
    @GET("/api/myGov/mapConfig")
    Call<ResponseMapConfig> getMapConfig();
    
    @GET("/api/myGov/allowZone/{lng}/{lat}")
    Call<ResponseAllowZone> isAllow(@Path("lng") Double longitud, @Path("lat") Double latitud);
    
    @GET("/api/myGov/usuario")
    Call<ResponseUsuario> getUsuario(@Header("nxtoken") String nxtoken);
    
    @POST("api/registro/basicSingUpGoogle?movil=1")
    @FormUrlEncoded
    Call<User2> signinGoogle(@Field("nombre")String nombre, @Field("apellido")String apellido, @Field("email")String correo, @Field("token_google") String googleId);
    
    
}
