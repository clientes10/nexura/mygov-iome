package com.nexura.calireporta.views.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.nexura.calireporta.R;
import com.nexura.calireporta.presenters.SigninPresenter;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.SigninView;

import mehdi.sakout.fancybuttons.FancyButton;

public class SigninActivity extends AppCompatActivity implements SigninView {
   TextInputLayout tilName, tilLastName, tilEmail, tilPassword;
   TextInputEditText etName, etLastName, etEmail, etPassword;
   FancyButton btnSignin;
   ProgressDialog progressDialog;
   SigninPresenter signinPresenter;
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_signin);
      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      initViews();
      
   }
   
   public void initViews(){
      signinPresenter = new SigninPresenter(this, this);
      tilName = findViewById(R.id.namecontainer);
      tilLastName = findViewById(R.id.lastnamecontainer);
      tilEmail = findViewById(R.id.mailcontainer);
      tilPassword = findViewById(R.id.passcontainer);
      etName = findViewById(R.id.name);
      etLastName = findViewById(R.id.lastname);
      etEmail = findViewById(R.id.mail);
      etPassword = findViewById(R.id.pass);
      btnSignin = findViewById(R.id.registrar);
      progressDialog = new ProgressDialog(this);
      progressDialog.setTitle("Cargando...");
      progressDialog.setMessage("Por favor, espere.");
      progressDialog.setCancelable(false);
      progressDialog.setIndeterminate(true);
      
      btnSignin.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick (View v) {
            signinPresenter.attemptSigin(etName.getText().toString(), etLastName.getText().toString(),etEmail.getText().toString(), etPassword.getText().toString());
         }
      });
   }
   
   @Override
   public void showProgress () {
      progressDialog.show();
   }
   
   @Override
   public void hideProgress () {
      progressDialog.dismiss();
   }
   
   @Override
   public void setUsernameError (String error) {
      tilEmail.setError(error);
   }
   
   @Override
   public void setPasswordError (String error) {
      tilPassword.setError(error);
   }
   
   @Override
   public void setSigninError (String error) {
      Utils.showToast(this,error);
   }
   
   @Override
   public void onSigninSuccess (String message) {
      Utils.showToast(this,message);
      Utils.setIntent(this,LoginActivity.class);
      finish();
   }
   
   @Override
   public void setNameError (String error) {
      tilName.setError(error);
   }
   
   @Override
   public void setLastNameError (String error) {
      tilLastName.setError(error);
   }

}
