package com.nexura.calireporta.views;

public interface SigninView {
   void showProgress();
   void hideProgress();
   void setUsernameError(String error);
   void setPasswordError(String error);
   void setSigninError(String error);
   void onSigninSuccess(String message);
   void setNameError (String error);
   void setLastNameError(String error);
   
}
