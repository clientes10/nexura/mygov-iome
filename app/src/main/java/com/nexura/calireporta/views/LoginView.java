package com.nexura.calireporta.views;

public interface LoginView {
   void showProgress();
   void hideProgress();
   void setUsernameError(String error);
   void setPasswordError(String error);
   void setLoginError(String error);
   void onNavigateHome();
   
   
}
