package com.nexura.calireporta.views;

public interface CompleteListener {
   void onSuccess();
   void onSuccess(String message);
   void onError(String error);
   
}
