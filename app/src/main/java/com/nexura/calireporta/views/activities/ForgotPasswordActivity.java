package com.nexura.calireporta.views.activities;

import android.app.ProgressDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;

import com.nexura.calireporta.R;
import com.nexura.calireporta.presenters.ForgotPasswordPresenter;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.ForgotPasswordView;

import mehdi.sakout.fancybuttons.FancyButton;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgotPasswordView {
   TextInputEditText etEmail;
   TextInputLayout tilEmail;
   ProgressDialog progressDialog;
   FancyButton btnSend;
   ForgotPasswordPresenter forgotPasswordPresenter;
   @Override
   protected void onCreate (Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_forgot_password);
      Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      initViews();
   }
   
   public void initViews(){
      progressDialog = new ProgressDialog(this);
      progressDialog.setTitle("Cargando...");
      progressDialog.setMessage("Por favor, espere.");
      progressDialog.setCancelable(false);
      progressDialog.setIndeterminate(true);
      forgotPasswordPresenter = new ForgotPasswordPresenter(this,this);
      etEmail = findViewById(R.id.mail);
      tilEmail = findViewById(R.id.mailcontainer);
      btnSend = findViewById(R.id.aceptar);
      btnSend.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick (View v) {
            forgotPasswordPresenter.attemptSendEmail(etEmail.getText().toString());
         }
      });
   }
   
   @Override
   public void showProgress () {
      progressDialog.show();
   }
   
   @Override
   public void hideProgress () {
      progressDialog.dismiss();
   }
   
   @Override
   public void setUsernameError (String error) {
      tilEmail.setError(error);
   }
   
   @Override
   public void setSendEmailError (String error) {
      Utils.showDialog(this,"Error",error);
   }
   
   @Override
   public void onSendEmailSuccess (String message) {
      Utils.showDialog(this,"Información",message);
      btnSend.setEnabled(false);
   }
}
