package com.nexura.calireporta.views;

public interface ForgotPasswordView  {
   void showProgress();
   void hideProgress();
   void setUsernameError(String error);
   void setSendEmailError(String error);
   void onSendEmailSuccess(String message);
}
