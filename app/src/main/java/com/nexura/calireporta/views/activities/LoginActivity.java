package com.nexura.calireporta.views.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.nexura.calireporta.R;
import com.nexura.calireporta.presenters.LoginPresenter;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.LoginView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private static final int RC_SIGN_IN = 223;
    TextInputEditText etEmail, etPassword;
    TextInputLayout tilEmail, tilPassword;
    TextView tvForgotPassword;
    FancyButton btnLogin,tvSignin;
    LoginPresenter loginPresenter;
    ProgressDialog progressDialog;
    ImageView btnLoginGoogle;
    ImageView btnLoginFacebook;
    Activity context;
    CallbackManager callbackManager;
    GoogleSignInClient mGoogleSigninClient;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    
        printKeyHash();
    }

    public void initViews(){
        context = this;
        callbackManager = CallbackManager.Factory.create();
        

        loginPresenter = new LoginPresenter(this,this);
        etEmail = findViewById(R.id.mail);
        etPassword = findViewById(R.id.pass);
        tilEmail = findViewById(R.id.mailcontainer);
        tilPassword = findViewById(R.id.passcontainer);
        tvForgotPassword = findViewById(R.id.olvidepass);
        btnLogin = findViewById(R.id.login);
        tvSignin = findViewById(R.id.register);
        btnLoginFacebook = findViewById(R.id.btnLoginFacebook);
        btnLoginGoogle = findViewById(R.id.btnLoginGoogle);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Cargando...");
        progressDialog.setMessage("Por favor, espere.");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        // Configure el inicio de sesión para solicitar la identificación del usuario, la dirección de correo electrónico y la información básica
// perfil. El ID y el perfil básico se incluyen en DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder (GoogleSignInOptions.DEFAULT_SIGN_IN)
              .requestEmail ()
              .build();
        mGoogleSigninClient = GoogleSignIn.getClient(context,gso);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                loginPresenter.attemptLogin(etEmail.getText().toString(),etPassword.getText().toString());
            }
        });
        tvSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Utils.setIntent(context,SigninActivity.class);
            }
        });
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                Utils.setIntent(context,ForgotPasswordActivity.class);
            }
        });
        
        btnLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {

                loginPresenter.attemptLoginFacebook(callbackManager);
            }
        });
    
    
        btnLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                loginPresenter.attemptLoginGoogle(mGoogleSigninClient);
            }
        });
    }
    
    /*
    
    public void openLogin(final Context context){
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
// inflate other xml where WebView is
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = layoutInflater.inflate(R.layout.content_login, null);
        email = (TextInputEditText)v.findViewById(R.id.mail);
        password = (TextInputEditText)v.findViewById(R.id.pass);
        final TextInputLayout containeremail = (TextInputLayout)v.findViewById(R.id.mailcontainer);
        final TextInputLayout containerpassword = (TextInputLayout)v.findViewById(R.id.passcontainer);
        final TextView textOlvide = (TextView)v.findViewById(R.id.olvidepass);
        FancyButton fancy_login = (FancyButton) v.findViewById(R.id.login);
        TextView textRegistrarse = (TextView) v.findViewById(R.id.register);
        alert.setView(v);
        alert.show();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                      loginResult.getAccessToken(),
                      new GraphRequest.GraphJSONObjectCallback() {
                          @Override
                          public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                              try {
                                  email_string = "";
                                  if (object.getString("email") != null) {
                                      email_string = object.getString("email");
                                  }
                                  // save profile information to preferences
                                  Log.e("MainActivity",object.getString("name")+object.getString("name")+email_string+loginResult.getAccessToken().getToken());
                                  myGovServices.nuevoUsuarioFacebook(object.getString("name"),object.getString("last_name"),email_string,loginResult.getAccessToken().getToken()).enqueue(new Callback<User2>() {
                                      @Override
                                      public void onResponse(Call<User2> call, Response<User2> response) {
                                          if(response.code()!=401)
                                              if(response.code()== 500){
                                                  Toast.makeText(context,response.message().toString(),Toast.LENGTH_SHORT).show();
                                              }else{
                                                  if(response.body().data.success){
                                                      sharedPrefs.saveSharedSetting("login.facebook",true);
                                                      sharedPrefs.saveSharedSetting("usuario",email_string);
                                                      sharedPrefs.saveSharedSetting("token.facebook",loginResult.getAccessToken().toString());
                                                      sharedPrefs.saveSharedSetting("logged",true);
                                                      startActivity(new Intent(MainActivity.this, SplashActivity.class));
                                                      
                                                  }else{
                                                      Toast.makeText(context,"OCURRIO UN ERROR AL INTENTAR INICIAR SESIÓN",Toast.LENGTH_SHORT).show();
                                                  }
                                              }
                                          else{
                                              Toast.makeText(context,"USUARIO NO AUTORIZADO",Toast.LENGTH_SHORT).show();
                                              LoginManager.getInstance().logOut();
                                          }
                                      }
                                      @Override
                                      public void onFailure(Call<User2> call, Throwable t) {
                                      }
                                  });
                                  // redirect to main screen
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }
                          }
                      });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,birthday,email,picture");
                request.setParameters(parameters);
                request.executeAsync();
                
                
            }
            
            @Override
            public void onCancel() {
                Toast.makeText(context,"SE HA CANCELADO EL INICIO DE SESIÓN",Toast.LENGTH_SHORT).show();
            }
            
            @Override
            public void onError(FacebookException e) {
                Toast.makeText(context,"ERROR"+e.getMessage().toString(),Toast.LENGTH_SHORT).show();
                
            }
        });
        
        
        
    }
    */
    
    
    @Override
    public void showProgress () {
        progressDialog.show();
    }
    
    @Override
    public void hideProgress () {
        progressDialog.dismiss();
    }
    
    @Override
    public void setUsernameError (String error) {
        tilEmail.setError(error);
    }
    
    @Override
    public void setPasswordError (String error) {
        tilPassword.setError(error);
    }
    
    @Override
    public void setLoginError (String error) {
        Utils.showDialog(this,"Error",error);
    }
    
    @Override
    public void onNavigateHome () {
        Utils.setIntent(this,SplashActivity.class);
        finish();
    }
    
    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.nexura.calireporta", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    protected void onStart () {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
    }
    
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if(account != null) {
                loginPresenter.attemptRegisterLoginApiGoogle(account.getGivenName(), account.getFamilyName(), account.getEmail(), account.getId());
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(LoginActivity.class.getName(), "signInResult:failed code=" + e.getStatusCode());
            Utils.showDialog(context, "Error",e.getLocalizedMessage());
        }
    }
}
