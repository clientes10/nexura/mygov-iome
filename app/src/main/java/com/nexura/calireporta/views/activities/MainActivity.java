package com.nexura.calireporta.views.activities;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.controllers.MapController;
import com.nexura.calireporta.app.MyApplication;
import com.nexura.calireporta.R;
import com.nexura.calireporta.adapters.TabAdapter;
import com.nexura.calireporta.fragments.MapFragment;
import com.nexura.calireporta.fragments.MyCasesFragment;
import com.nexura.calireporta.fragments.RecentFragment;
import com.nexura.calireporta.models.Categoria;
import com.nexura.calireporta.models.Comuna;
import com.nexura.calireporta.models.Estado;
import com.nexura.calireporta.models.EventoSingle;
import com.nexura.calireporta.models.Localidad;
import com.nexura.calireporta.models.ResponseEventos2;
import com.nexura.calireporta.models.ResponseEventosAfecta2;
import com.nexura.calireporta.models.ResponseMapConfig;
import com.nexura.calireporta.models.SubCategoria;
import com.nexura.calireporta.models.ui.LocalidadSpinnerItem;
import com.nexura.calireporta.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,
        MyCasesFragment.OnFragmentInteractionListener,
        RecentFragment.OnFragmentInteractionListener,
        MapFragment.OnFragmentInteractionListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
{
    ProfileTracker profileTracker;
    LoginButton loginButton;
    CallbackManager callbackManager;
    EditText fecha;
    Spinner locaSpinner;
    Spinner cat;
    Spinner subCat;
    Spinner comuSpinner;
    Spinner estaSpinner;
    String email_string;
    private LinearLayout container_login;
    private LinearLayout container_eventos;
    private FancyButton btn_login;
    private ProgressDialog dialog;
    private MyCasesFragment.OnFragmentInteractionListener mListener;
    MyApplication _app = MyApplication.getInstance();
    public Context context;
    private TextInputEditText email,password;
    private TabAdapter mSectionsPagerAdapter;
    private TabLayout tabLayout;
    private String TAG = "MyGov";
    private Menu menu;
    private int LOGIN_REQUEST=111;
    private int COMENTARIO_OK = 222;
    MyCasesFragment myCasesFragment;
    MapFragment mapFragment;
    RecentFragment recentFragment;
    private ViewPager mViewPager;
    MapController mapController;
    Dialog filter;
    SharedPrefs sharedPrefs;
    private GoogleApiClient googleApiClient;
    String filcatid="",fillocid="",filfecha="",filestid="",filsubid="";
    double lat = 3.452139,lon = -76.531002;
    boolean isMyLocationActive = false;
    boolean isFilterActive=true;
    List<ImageView> imgs = new ArrayList<>();
    int REQUEST_NEW_EVENT=345;
    ProgressDialog pd;
    Retrofit retrofit;
    MyGovServices myGovServices;
    int aux_cat, aux_subcata, aux_comuna, aux_barrio,aux_estado,aux_localidad;
    String aux_fecha;
    TextView nombreUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        dialog = new ProgressDialog(context);
        sharedPrefs = new SharedPrefs(context);
    
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
              this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        callbackManager = CallbackManager.Factory.create();
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged (Profile profile, Profile profile1) {
    
            }
        };
    
    
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    
        mSectionsPagerAdapter = new TabAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled (int position, float positionOffset, int positionOffsetPixels) {
                //changeTitle(position);
            }
    
            @Override
            public void onPageSelected (int position) {
                changeTitle(position);
                try {
                    changeMenu(position);
                } catch (Exception ex) {
                    Log.d(TAG, "first time open");
                }
            }
    
            @Override
            public void onPageScrollStateChanged (int state) {
        
            }
        });
    
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        createCustomIcons();
        mViewPager.setCurrentItem(2);
        myCasesFragment = mSectionsPagerAdapter.myCasesFragment;
        mapFragment = mSectionsPagerAdapter.mapFragment;
        recentFragment = mSectionsPagerAdapter.recentFragment;
        googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();
        initRetrofit();
        hideOrShowItemSession();
        View headerView = navigationView.getHeaderView(0);
        nombreUsuario = (TextView) headerView.findViewById(R.id.nombreUsuario);
        if (sharedPrefs.readSharedSetting("logged", false)) {
            nombreUsuario.setText(sharedPrefs.readSharedSetting("email",""));
        } else {
            nombreUsuario.setVisibility(View.GONE);
        }
    }
    public void initRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.server))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myGovServices = retrofit.create(MyGovServices.class);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    public void createCustomIcons()
    {
        SpannableString spannablecontent=new SpannableString("Mis alertas");
        spannablecontent.setSpan(new RelativeSizeSpan(0.8f),
                0,spannablecontent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_title, null);
        tabOne.setText(spannablecontent);

        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.account, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        spannablecontent=new SpannableString("Mapa");
        spannablecontent.setSpan(new RelativeSizeSpan(0.8f),
                0,spannablecontent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_title, null);
        tabTwo.setText(spannablecontent);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.flag, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        spannablecontent=new SpannableString("Recientes");
        spannablecontent.setSpan(new RelativeSizeSpan(0.8f),
                0,spannablecontent.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab_title, null);
        tabThree.setText(spannablecontent);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.map, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
        
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu=menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            if(sharedPrefs.readSharedSetting("logged",false)){// SI HAY USUARIO LOGGEADO
                startNewEvent();
            }else{//SI NO HAY USUARIO LOGGEADO
                //PANTALLA DE LOGIN
                Utils.setIntent(context,LoginActivity.class);
            }
            return true;
        }
        else if (id == R.id.action_position) {
            if(isMyLocationActive)
            {
                Drawable icon = menu.getItem(1).getIcon();
                icon.mutate();
                icon.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
                menu.getItem(1).setIcon(icon);
                mapController.refreshMarkers();
                isMyLocationActive=false;
            }
            else
            {
                Drawable icon = menu.getItem(1).getIcon();
                icon.mutate();
                icon.setColorFilter(Color.parseColor("#ff0000"), PorterDuff.Mode.SRC_ATOP);
                menu.getItem(1).setIcon(icon);
                mapController.myLocationMarkers(new LatLng(lat,lon));
                isMyLocationActive=true;
            }
            return true;
        }
        else if (id == R.id.action_filter) {
            showFilter();
            Drawable icon = menu.getItem(2).getIcon();
            icon.mutate();
            icon.setColorFilter(Color.parseColor("#ff0000"), PorterDuff.Mode.SRC_ATOP);
            menu.getItem(2).setIcon(icon);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startNewEvent()
    {
        Intent i = new Intent(this, NewCaseActivity.class);
        i.putExtra("lat",lat);
        i.putExtra("lon",lon);
        startActivityForResult(i,REQUEST_NEW_EVENT);
    }

    private void changeMenu(int position)
    {
        switch (position)
        {
            case 0:
                menu.getItem(0).setVisible(true);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                break;
            case 1:
                menu.getItem(0).setVisible(false);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(false);
                break;
            case 2:
                menu.getItem(0).setVisible(false);
                menu.getItem(1).setVisible(true);
                menu.getItem(2).setVisible(true);
                break;
            case 3:
                menu.getItem(0).setVisible(false);
                menu.getItem(1).setVisible(false);
                menu.getItem(2).setVisible(true);
                break;
        }
    }

    private void changeTitle(int position){
        switch (position){
            case 0:
                getSupportActionBar().setTitle("Mis alertas");
                break;
            case 1:
                getSupportActionBar().setTitle("Me afectan");
                break;
            case 2:
                getSupportActionBar().setTitle("Mapa");
                break;
            case 3:
                getSupportActionBar().setTitle("Recientes");
                break;
        }
    }
    public void addEventOpen(){
        if(sharedPrefs.readSharedSetting("logged",false)){// SI HAY USUARIO LOGGEADO
            //ENVIAR COMENTARIO
            startNewEvent();
        }else{//SI NO HAY USUARIO LOGGEADO
            //PANTALLA DE LOGIN
            Utils.setIntent(context,LoginActivity.class);
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            addEventOpen();
        } else if (id == R.id.nav_position) {
            if(isMyLocationActive){
                Drawable icon = menu.getItem(1).getIcon();
                icon.mutate();
                icon.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
                menu.getItem(1).setIcon(icon);
                mapController.refreshMarkers();
                isMyLocationActive=false;
            }
            else{
                Drawable icon = menu.getItem(1).getIcon();
                icon.mutate();
                icon.setColorFilter(Color.parseColor("#ff0000"), PorterDuff.Mode.SRC_ATOP);
                menu.getItem(1).setIcon(icon);
                mapController.myLocationMarkers(new LatLng(lat,lon));
                isMyLocationActive=true;
            }

        } else if(id == R.id.nav_filter){
            showFilter();
            Drawable icon = menu.getItem(2).getIcon();
            icon.mutate();
            icon.setColorFilter(Color.parseColor("#ff0000"), PorterDuff.Mode.SRC_ATOP);
            menu.getItem(2).setIcon(icon);
        } else if (id == R.id.nav_login) {
            Utils.setIntent(context,LoginActivity.class);
        } else if (id == R.id.nav_help) {
        }else if(id == R.id.nav_exit){
            sharedPrefs.saveSharedSetting("logged",false);
            sharedPrefs.saveSharedSetting("userId",null);
            sharedPrefs.saveSharedSetting("token",null);
            sharedPrefs.saveSharedSetting("email",null);
            sharedPrefs.saveSharedSetting("passwd",null);
            sharedPrefs.saveSharedSetting("typeLogged","");
            sharedPrefs.saveSharedSetting("loadEventos", false);
            sharedPrefs.saveSharedSetting("eventos", null);
            sharedPrefs.saveSharedSetting("loadMisEventos",false);
            sharedPrefs.saveSharedSetting("misEventos","");
            sharedPrefs.saveSharedSetting("loadComunas",false);
            sharedPrefs.saveSharedSetting("comunas","");
            sharedPrefs.saveSharedSetting("loadCategorias",false);
            sharedPrefs.saveSharedSetting("categorias",null);
            sharedPrefs.saveSharedSetting("loadSubCategorias",false);
            sharedPrefs.saveSharedSetting("subcategorias",null);
            sharedPrefs.saveSharedSetting("loadEstados",false);
            sharedPrefs.saveSharedSetting("estados",null);
            sharedPrefs.saveSharedSetting("loadTipoValoraciones",false);
            sharedPrefs.saveSharedSetting("tipovaloraciones",null);
            sharedPrefs.saveSharedSetting("loadMapConfig",false);
            sharedPrefs.saveSharedSetting("mapconfig",null);
            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(intent);
            
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showFilter()
    {
        try
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = layoutInflater.inflate(R.layout.dialog_filter, null);

            final Dialog dialog = new Dialog(this);
            filter = dialog;
            dialog.setContentView(R.layout.dialog_filter);
            dialog.setTitle("Filtrar");
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH);
            int month = c.get(Calendar.MONTH);
            int year = c.get(Calendar.YEAR);
            fecha = (EditText)dialog.findViewById(R.id.fecha);
            locaSpinner = (Spinner)dialog.findViewById(R.id.barrio);
            cat = (Spinner)dialog.findViewById(R.id.categoria);
            subCat = (Spinner)dialog.findViewById(R.id.subcategoria);
            comuSpinner = (Spinner)dialog.findViewById(R.id.comuna);
            estaSpinner = (Spinner)dialog.findViewById(R.id.estado);
            final FancyButton limpiar = (FancyButton)dialog.findViewById(R.id.limpiar);
            final FancyButton aplicar = (FancyButton)dialog.findViewById(R.id.aplicar);
            fecha.setInputType(InputType.TYPE_NULL);
            final DatePickerDialog datePickerDialog = new DatePickerDialog(dialog.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    String dia = String.valueOf(day);
                    String mes = String.valueOf(month+1);
                    String año = String.valueOf(year);

                    if(dia.length()<2)
                        dia = "0"+dia;
                    if(mes.length()<2)
                        mes = "0"+mes;
                    fecha.setText(dia+"/"+mes+"/"+año);
                }
            },year,month,day);
            fecha.setClickable(true);
            fecha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datePickerDialog.show();
                }
            });
            final List<String> spinnerCategoria =  new ArrayList<>();
            spinnerCategoria.add("TODAS");
            for (Categoria x:MyApplication.getCategorias()) {
                spinnerCategoria.add(x.nombre);
                Log.d("CATS",x.nombre);
            }
            

            ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(
                    dialog.getContext(), android.R.layout.simple_spinner_item, spinnerCategoria);
            adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            cat.setAdapter(adapterCat);
            cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
                    loadSubCat(selectedItemText,subCat,dialog.getContext());
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            List<String> dataComunas = new ArrayList<>();
            dataComunas.add(0,"TODAS");
            List<Comuna> objComunas = MyApplication.getComunas();
            for (int i=0; i < objComunas.size();i++)
            {
                dataComunas.add(objComunas.get(i).comuna_nombre);
                Log.d("COMUNAS",String.valueOf(objComunas.get(i).comuna_id));
            }
            /*Collections.sort(dataComunas, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.replace("COMUNA ","").compareTo(o2.replace("COMUNA ",""));
                }
            });*/
            ArrayAdapter<String> adapterComunas = new ArrayAdapter<String>(dialog.getContext(),
                    android.R.layout.simple_spinner_item,dataComunas);
            adapterComunas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            comuSpinner.setAdapter(adapterComunas);
            comuSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItemText = (String) parent.getItemAtPosition(position);
                    Log.e("COMUNA SELECTED",String.format("%s",position));
                    loadLocalidades(position,locaSpinner,dialog.getContext());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
    
            List<Estado> listSpinnerCategoria = new ArrayList<>();
            listSpinnerCategoria.add(new Estado(0,"TODOS"));
            for (Estado x:MyApplication.getEstados()) {
                listSpinnerCategoria.add(x);
                Log.d("ESTADO",x.nombre);
            }
            ArrayAdapter<Estado> adapterEstados = new ArrayAdapter<Estado>(dialog.getContext(),
                    android.R.layout.simple_spinner_item,listSpinnerCategoria);
            
            adapterEstados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            estaSpinner.setAdapter(adapterEstados);

            limpiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    limpiar(dialog);
                }
            });
            aplicar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int catID = -1;
                    int subCatID = -1;
                    int comunaID = -1;
                    int localidadID = -1;
                    int estadoID = -1;
                    String fechaString = "";
                    int id_subcat = subCat.getSelectedItemPosition();
                    String auxCat = (String)cat.getSelectedItem();
                    String auxSubCat = (String)subCat.getSelectedItem();
                    String auxComuna = (String)comuSpinner.getSelectedItem();
                    Estado objItemEstado = (Estado)estaSpinner.getSelectedItem();
                    String auxLocalidad = "";
                    LocalidadSpinnerItem objItemLocalidad = (LocalidadSpinnerItem)locaSpinner.getSelectedItem();
                    LocalidadSpinnerItem objComp = new LocalidadSpinnerItem("TODAS",-1);
                    if (objItemLocalidad != objComp && objItemLocalidad !=null)
                         auxLocalidad = objItemLocalidad.name;
                    String auxEstado = objItemEstado.nombre;


                    aux_cat = cat.getSelectedItemPosition();
                    aux_subcata = subCat.getSelectedItemPosition();
                    aux_comuna = comuSpinner.getSelectedItemPosition();
                    aux_fecha = fecha.getText().toString();
                    aux_estado = estaSpinner.getSelectedItemPosition();
                    aux_localidad = locaSpinner.getSelectedItemPosition();
                    if(!fecha.getText().toString().isEmpty()){
                        fechaString = fecha.getText().toString();
                    }
                    if(auxCat.contains("TODAS"))
                    {

                    }else {

                        catID = MyApplication.getCategorias().get(aux_cat-1).id;
                        if(auxSubCat.contains("TODAS")) {

                        }else{
                            if (MyApplication.getCategoriasconId(catID).subcategorias != null)
                                if (MyApplication.getCategoriasconId(catID).subcategorias.size() > 0)
                                    subCatID = MyApplication.getCategoriasconId(catID).subcategorias.get(aux_subcata - 1).id;
                        }
                    }

                    if(!auxComuna.contains("TODAS")){
                        comunaID = MyApplication.getComunas().get(aux_comuna -1).comuna_id;
                    }

                    if(!auxLocalidad.contains("TODAS") && !auxLocalidad.isEmpty()){
                        localidadID = MyApplication.getComunas().get(aux_comuna -1).localidades.get(aux_localidad-1).id;
                    }

                    if(!auxEstado.contains("TODOS")){
                        estadoID = MyApplication.getEstados().get(aux_estado-1).id;
                    }
                    Log.e("VALOR CATEGORIA",String.valueOf(catID));
                    Log.e("VALOR SUBCATEGORIA",String.valueOf(subCatID));
                    Log.e("VALOR COMUNA",String.valueOf(comunaID));
                    Log.e("VALOR LOCALIDAD", String.valueOf(localidadID));
                    Log.e("VALOR ESTADO", String.valueOf(estadoID));
                    Log.e("VALOR FECHA",fechaString);
                    aplicarFiltro(dialog, catID,subCatID , comunaID, localidadID,estadoID,fechaString);
                }
            });


            if(sharedPrefs.readSharedSetting("isFiltered",false)){
                cat.setSelection(aux_cat,true);
                comuSpinner.setSelection(aux_comuna,true);
                fecha.setText(aux_fecha);
                estaSpinner.setSelection(aux_estado,true);
            }

            dialog.show();
        }
        catch (Exception e)
        {
            Log.e("ERR",e.toString());
        }
    }

    public void aplicarFiltro(Dialog dialog,int cat,int subCat,int com,int loc, int estado, String fecha)
    {
        List<EventoSingle> datos = MyApplication.getEventosBackup();
        List<EventoSingle> aux = new ArrayList<>();
        List<EventoSingle> auxTwo = new ArrayList<>();
        if (cat!=-1&&subCat!=-1)
        {
            for (EventoSingle x:datos)
            {
                if(x.categoria.id==subCat)
                {
                    auxTwo.add(x);
                }
            }
            aux.clear();
            aux.addAll(auxTwo);
        }else if (cat!=-1&&subCat==-1){
            for (EventoSingle x:datos)
            {
                if(x.categoria.idpadre==cat)
                {
                    auxTwo.add(x);
                }
            }
            aux.clear();
            aux.addAll(auxTwo);
        }
        if(com!=-1){
            auxTwo.clear();
            if(aux.isEmpty()) {
                for (EventoSingle x : datos)
                    if (x.localidad.comuna_id == com)
                        auxTwo.add(x);
            }
            else{
                for(EventoSingle x: aux)
                    if(x.localidad.comuna_id == com)
                        auxTwo.add(x);
            }
            aux.clear();
            aux.addAll(auxTwo);
        }
        if(loc != -1){
            auxTwo.clear();
            if(aux.isEmpty()){
                for(EventoSingle x: datos)
                    if(x.localidad.id == loc)
                        auxTwo.add(x);
            }else{
                for(EventoSingle x: aux)
                    if(x.localidad.id == loc)
                        auxTwo.add(x);
            }
            aux.clear();
            aux.addAll(auxTwo);
        }
        if(estado != -1){
            auxTwo.clear();
            if(aux.isEmpty()){
                for(EventoSingle x: datos)
                    if(x.estado.id == estado)
                        auxTwo.add(x);
            }else{
                for(EventoSingle x: aux)
                    if(x.estado.id == estado)
                        auxTwo.add(x);
            }
            aux.clear();
            aux.addAll(auxTwo);
        }
        if(!fecha.isEmpty()){
         auxTwo.clear();
            Log.e("FECHA","entra");

            if(aux.isEmpty()){
             for(EventoSingle x : datos) {
                 Log.e("FECHA EVENTOS",x.fecha);
                 if (x.fecha.contains(fecha)) {
                     auxTwo.add(x);
                     Log.e("FECHA", "DATOS");
                 }
             }
         }else{
             for(EventoSingle x : aux) {
                 Log.e("FECHA EVENTOS",x.fecha);

                 if (x.fecha.contains(fecha)) {
                     auxTwo.add(x);
                     Log.e("FECHA", "AUX");

                 }
             }
         }
            aux.clear();
            aux.addAll(auxTwo);
        }

        MyApplication.setEventos(aux);
        sharedPrefs.saveSharedSetting("isFiltered",true);
        mSectionsPagerAdapter.recentFragment.refresh();
        mapController.refresh(aux);
        if(cat == -1 && subCat == -1 && com == -1 && loc == -1 && estado == -1 && fecha.isEmpty()){
            MyApplication.setEventos(MyApplication.getEventosBackup());
            sharedPrefs.saveSharedSetting("isFiltered",false);
            mSectionsPagerAdapter.recentFragment.refresh();
            mapController.refresh(MyApplication.getEventosBackup());
        }
        dialog.dismiss();
    }

    public void limpiar(Dialog dialog)
    {
        MyApplication.setEventos(MyApplication.getEventosBackup());
        sharedPrefs.saveSharedSetting("isFiltered",false);
        mSectionsPagerAdapter.recentFragment.refresh();
        dialog.dismiss();
        Drawable icon = menu.getItem(2).getIcon();
        icon.mutate();
        icon.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
        menu.getItem(2).setIcon(icon);
        mapController.refresh(MyApplication.getEventosBackup());
    }

    public void loadSubCat(String cat, Spinner subCats, Context dialog)
    {
        if(cat.equals("TODAS"))
        {
            subCats.setAdapter(null);
        }
        else
        {
            for (Categoria x:MyApplication.getCategorias())
            {
                if(x.nombre.equals(cat))
                {
                    List<String> spinnerCategoria =  new ArrayList<>();
                    spinnerCategoria.add("TODAS");
                    for (SubCategoria y:x.subcategorias) {
                        spinnerCategoria.add(y.nombre);
                        Log.d("CATS",y.nombre);
                    }
                    ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(
                            dialog, android.R.layout.simple_spinner_item, spinnerCategoria);
                    adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    subCats.setAdapter(adapterCat);
                    if(sharedPrefs.readSharedSetting("isFiltered",false)){
                        subCats.setSelection(aux_subcata,true);
                    }
                    break;
                }
            }
        }
    }

    public void loadLocalidades(int comuna,Spinner localidades,Context dialog)
    {
        if(comuna == 0)
        {
            localidades.setAdapter(null);
        }
        else
        {
            List<Localidad> aux = MyApplication.getComunas().get(comuna-1).localidades;
            ArrayList<LocalidadSpinnerItem> items  = new ArrayList<>();
            items.add(new LocalidadSpinnerItem("TODAS",-1));
            for (Localidad x : aux)
            {
                LocalidadSpinnerItem y = new LocalidadSpinnerItem(x.nombre,x.id);
                items.add(y);
            }
            ArrayAdapter<LocalidadSpinnerItem> adapterCat = new ArrayAdapter<LocalidadSpinnerItem>(
                    dialog, android.R.layout.simple_spinner_item, items);
            adapterCat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            localidades.setAdapter(adapterCat);
            if(sharedPrefs.readSharedSetting("isFiltered",false))
                locaSpinner.setSelection(aux_localidad,true);

        }
    }


    /**
     * Interface for fragment interaction ( MyCasesFragment )
     * @param action
     */
    @Override
    public void onFragmentInteraction(String action) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {

        if (requestCode == LOGIN_REQUEST) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "LOGGED IN", Toast.LENGTH_SHORT).show();

                mSectionsPagerAdapter.myCasesFragment.refresh();
                mSectionsPagerAdapter.recentFragment.refresh();
    
                loadMapConfig();

            }
            else
            {
                Toast.makeText(this, "NOT LOGGED IN", Toast.LENGTH_SHORT).show();
            }
        }
        else if(requestCode==REQUEST_NEW_EVENT&&resultCode==RESULT_OK)
        {
            Toast.makeText(this, "se ha creado el evento correctamente", Toast.LENGTH_SHORT).show();
            loadMapConfig();
        }else if(requestCode==COMENTARIO_OK&&resultCode==RESULT_OK){
            mSectionsPagerAdapter.myCasesFragment.refresh();
            mSectionsPagerAdapter.recentFragment.refresh();
    
            loadMapConfig();
        }
        /**
        for (android.support.v4.app.Fragment fragment : getSupportFragmentManager().getFragments()) {
            //System.out.println("@#@");
            fragment.onActivityResult(requestCode, resultCode, data);
        }**/
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }


    /**
     * Interface for fragment interaction ( MeAfectanFragment )
     * @param uri
     */
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Interface for fragment interaction ( MapFragment )
     * @param map
     */
    @Override
    public void onFragmentInteraction(GoogleMap map) {
        //MyApplication = MyApplication.getInstance();
        List<EventoSingle> eventos = MyApplication.getEventos();
        mapController = new MapController(map,eventos,this);
        mapController.addMarkers();
        mapController.createMarkerEvent();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            try
            {
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                lat = lastLocation.getLatitude();
                lon = lastLocation.getLongitude();
                Log.d("SIBICA", lat+" "+lon);
            }
            catch (Exception e)
            {
                lat = 3.4338878605532823;
                lon = -76.52428268897995;
                Log.d("SIBICA -  ERROR", lat+" "+lon);
            }

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public void showProgress()
    {
        pd = new ProgressDialog(this);
        pd.setTitle("Cargando...");
        pd.setMessage("Por favor, espere.");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        pd.show();
    }
    
    public void loadMapConfig(){
        Call<ResponseMapConfig> responseMapConfigCall = myGovServices.getMapConfig();
        responseMapConfigCall.enqueue(new Callback<ResponseMapConfig>() {
            @Override
            public void onResponse (Call<ResponseMapConfig> call, Response<ResponseMapConfig> response) {
                MyApplication.setMapConfig(response.body().data);
                //sharedPrefs.saveSharedSetting("loadMapConfig",true);
                Gson gson = new Gson();
                ResponseMapConfig responseMapConfig = response.body();
                String mapConfig = gson.toJson(responseMapConfig);
                Log.e("MapConfig",mapConfig);
                sharedPrefs.saveSharedSetting("mapconfig",mapConfig);
                loadMisEventos();
            }
            
            @Override
            public void onFailure (Call<ResponseMapConfig> call, Throwable throwable) {
                loadMisEventos();
            }
        });
    }
    public void loadMisEventos()
    {
        try
        {
            if (isOnline(this))
            {
                Call<ResponseEventos2> getEventos = myGovServices.getEventosMios(MyApplication.getUser().token);
                getEventos.enqueue(new Callback<ResponseEventos2>() {
                    @Override
                    public void onResponse(Call<ResponseEventos2> call, Response<ResponseEventos2> response) {
                        if(response.isSuccessful())
                        {
                            try
                            {
                                MyApplication.setMisEventos(response.body().data.eventos);
                                MyApplication.setMisEventosBackup(response.body().data.eventos);
                            }
                            catch (Exception e)
                            {
                                Log.d(TAG,e.toString());
                            }
                            //pd.dismiss();
                            //initMain();

                            Gson gson = new Gson();
                            ResponseEventos2 resp = response.body();
                            String events = gson.toJson(resp);
                            sharedPrefs.saveSharedSetting("loadMisEventos",true);
                            sharedPrefs.saveSharedSetting("misEventos",events);
                            loadEventos();
                            mSectionsPagerAdapter.myCasesFragment.refresh();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEventos2> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " EVENTS-E", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("misEventos",null);
                if(stored!=null)
                {

                }
                ResponseEventos2 res = gson.fromJson(stored,ResponseEventos2.class);
                MyApplication.setMisEventosBackup(res.data.eventos);
                MyApplication.setMisEventos(res.data.eventos);
                loadEventos();
                mSectionsPagerAdapter.myCasesFragment.refresh();

            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadMisEventos",e.toString());
        }


    }

    public void loadEventos()
    {
        try
        {
            if(isOnline(this))
            {
                Call<ResponseEventos2> getEventos = myGovServices.getEventos();
                getEventos.enqueue(new Callback<ResponseEventos2>() {
                    @Override
                    public void onResponse(Call<ResponseEventos2> call, Response<ResponseEventos2> response) {
                        if(response.isSuccessful()) {
                            try {
                                MyApplication.setEventos(response.body().data.eventos);
                                MyApplication.setEventosBackup(response.body().data.eventos);
                            } catch (Exception e) {
                                Log.d(TAG, e.toString());
                            }
                            //pd.dismiss();
                            //initMain();
                            //loadComunas();
                            Gson gson = new Gson();
                            ResponseEventos2 resp = response.body();
                            String events = gson.toJson(resp);
                            sharedPrefs.saveSharedSetting("loadEventos", true);
                            sharedPrefs.saveSharedSetting("eventos", events);
                            mSectionsPagerAdapter.recentFragment.refresh();
                            mSectionsPagerAdapter.mapFragment.refresh();
    
                        }else{
                            loadEventos();
                        }
                    }
                    @Override
                    public void onFailure(Call<ResponseEventos2> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Verifique su conexion a internet", Toast.LENGTH_SHORT).show();
                        Log.e(TAG + " EVENTS-E", t.toString());
                    }
                });
            }
            else
            {
                Gson gson = new Gson();
                String stored = sharedPrefs.readSharedSetting("eventos",null);
                if(stored!=null)
                {

                }
                ResponseEventos2 res = gson.fromJson(stored,ResponseEventos2.class);
                MyApplication.setEventos(res.data.eventos);
                MyApplication.setEventosBackup(res.data.eventos);
                Log.e("LOADEVENTOS","SE HAN CARGADO LOS EVENTOS GUARDADOS");
                mSectionsPagerAdapter.recentFragment.refresh();
                mSectionsPagerAdapter.mapFragment.refresh();
    
            }
        }
        catch (Exception e)
        {
            Log.e("ERR - loadEventos",e.toString());
        }

    }



    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void hideOrShowItemSession(){
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        Menu nav_menu = navigationView.getMenu();
        if(sharedPrefs.readSharedSetting("logged",false)) {
            nav_menu.findItem(R.id.nav_login).setVisible(false);
            nav_menu.findItem(R.id.nav_exit).setVisible(true);
        }else {
            nav_menu.findItem(R.id.nav_login).setVisible(true);
            nav_menu.findItem(R.id.nav_exit).setVisible(false);
        }


    }

}
