package com.nexura.calireporta.controllers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nexura.calireporta.R;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.activities.SingleViewActivity;
import com.nexura.calireporta.models.EventoSingle;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by javpoblano on 26/03/17.
 */

public class MapController {
    private GoogleMap map;
    private Activity context;
    private List<EventoSingle> eventos;
    private HashMap<String,EventoSingle> markers;
    private List<Marker> mapMarkers;
    Circle circle;

    public MapController(GoogleMap map, List<EventoSingle> eventos,Activity context) {
        this.map = map;
        this.eventos = eventos;
        this.context = context;
    }

    public void refresh(List<EventoSingle> eventos)
    {
        this.eventos=eventos;
        map.clear();
        addMarkers();
        createMarkerEvent();
    }

    public void addMarkers()
    {
        map.clear();
        markers = new HashMap<>();
        mapMarkers = new ArrayList<>();
        for (int i = 0; i<eventos.size();i++) {
            Marker x = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(Double.parseDouble(eventos.get(i).y),Double.parseDouble(eventos.get(i).x)))

                                    );
            markers.put(x.getId(),eventos.get(i));
            mapMarkers.add(x);
        }
    }

    public void myLocationMarkers(LatLng center)
    {
        circle = map.addCircle(new CircleOptions()
                .center(center)
                .radius(1000)
                .strokeColor(Color.RED)
                .fillColor(Color.parseColor("#55ff0000")));
        for(Marker x : mapMarkers)
        {
            if(SphericalUtil.computeDistanceBetween(center,x.getPosition())>1000)
            {
                x.setVisible(false);
            }
        }
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(center,14));
    }

    public void refreshMarkers(){
        circle.remove();
        for(Marker x: mapMarkers)
        {
            x.setVisible(true);
        }
    }

    public void createMarkerEvent()
    {
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                EventoSingle evt = markers.get(marker.getId());
                SharedPrefs sharedPrefs = new SharedPrefs(context);
                if(!sharedPrefs.readSharedSetting("userId","").contains(evt.usuario_id)){
                    View view = context.getLayoutInflater().inflate(R.layout.layout_detail_alert, null);
                    TextView titulo = view.findViewById(R.id.titulo);
                    TextView categoria = view.findViewById(R.id.categoria);
                    TextView estado = view.findViewById(R.id.estado);
                    TextView fecha = view.findViewById(R.id.fecha);
                    titulo.setText(evt.titulo);
                    categoria.setText(evt.categoria.nombre);
                    estado.setText(evt.estado.nombre);
                    fecha.setText(evt.fecha);
                    Utils.showDialog(context,"Detalle",view);
                    return true;
                }
                Intent i = new Intent(context, SingleViewActivity.class);
                i.putExtra("id", evt.id);
                i.putExtra("from",1);
                context.startActivity(i);
                return false;
            }
        });

    }
}
