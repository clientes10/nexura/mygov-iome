package com.nexura.calireporta.presenters;

import android.content.Context;

import com.google.gson.Gson;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.models.Error;
import com.nexura.calireporta.models.ResponserecuperarPassword;
import com.nexura.calireporta.views.CompleteListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nexura.calireporta.utils.Utils.initRetrofit;

public class ForgotPasswordInteractor {
   CompleteListener listener;
   Context context;
   MyGovServices myGovServices;
   
   ForgotPasswordInteractor(CompleteListener listener){
      this.listener = listener;
   }
   
   public void performSendEmail(String email){
      myGovServices = initRetrofit(context);
      Call<ResponserecuperarPassword> responserecuperarPasswordCall = myGovServices.recuperarPassword(email);
      responserecuperarPasswordCall.enqueue(new Callback<ResponserecuperarPassword>() {
         @Override
         public void onResponse (Call<ResponserecuperarPassword> call, Response<ResponserecuperarPassword> response) {
            if(response.isSuccessful()){
               if(response.body().data.success){
                  listener.onSuccess(response.body().data.message);
               } else {
                  listener.onError(response.body().error.toString());
               }
            } else {
               Gson gson = new Gson();
               Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
               listener.onError(error.error.message);
            }
         }
   
         @Override
         public void onFailure (Call<ResponserecuperarPassword> call, Throwable throwable) {
            listener.onError(throwable.getMessage());
         }
      });
   }
   
}
