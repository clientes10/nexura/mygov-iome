package com.nexura.calireporta.presenters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.gson.Gson;
import com.nexura.calireporta.SharedPrefs;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.models.Error;
import com.nexura.calireporta.models.ResponseUsuario;
import com.nexura.calireporta.models.User;
import com.nexura.calireporta.models.User2;
import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.CompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.nexura.calireporta.utils.Utils.initRetrofit;

public class LoginInteractor  {
   private CompleteListener listener;
   MyGovServices myGovServices;
   Activity context;
   SharedPrefs sharedPrefs;
   String facebookId = "";
   String name = "";
   String last_name = "";
   String email = "";
   private static final int RC_SIGN_IN = 223;
   
   LoginInteractor(CompleteListener listener){
      this.listener = listener;
      
   }
   
   public void performLogin(final String username, final String password){
      myGovServices = initRetrofit(context);
      sharedPrefs = new SharedPrefs(context);
      Call<User2> user2Call = myGovServices.login("CMS",username,password);
      user2Call.enqueue(new Callback<User2>() {
         @Override
         public void onResponse (Call<User2> call, Response<User2> response) {
            if (response.isSuccessful()){
                  sharedPrefs.saveSharedSetting("email", username);
                  sharedPrefs.saveSharedSetting("typeLogged","CMS");
                  sharedPrefs.saveSharedSetting("passwd",password);
                  sharedPrefs.saveSharedSetting("logged", true);
                  sharedPrefs.saveSharedSetting("token", response.body().data.token);
                  myGovServices.getUsuario(sharedPrefs.readSharedSetting("token","")).enqueue(new Callback<ResponseUsuario>() {
                     @Override
                     public void onResponse (Call<ResponseUsuario> call, Response<ResponseUsuario> response) {
                        sharedPrefs.saveSharedSetting("userId",response.body().data.usuario.id);
                        listener.onSuccess();
                     }
   
                     @Override
                     public void onFailure (Call<ResponseUsuario> call, Throwable throwable) {
      
                     }
                  });
            } else {
               Gson gson = new Gson();
               Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
               listener.onError(error.error.message);
            }
         }
   
         @Override
         public void onFailure (Call<User2> call, Throwable throwable) {
            listener.onError(throwable.getMessage());
         }
      });
   }
   
   public void performLoginFacebook(CallbackManager callbackManager ){
      myGovServices = initRetrofit(context);
      sharedPrefs = new SharedPrefs(context);
      
      
      LoginManager.getInstance().registerCallback(callbackManager,
            new FacebookCallback<LoginResult>() {
               @Override
               public void onSuccess(final LoginResult loginResult) {
                  GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                           @Override
                           public void onCompleted(final JSONObject object, GraphResponse response) {

                              try {
                                  facebookId = object.getString("id");
                                  name = object.getString("first_name");
                                  last_name = object.getString("last_name");
                                  email = object.getString("email");
                                  //return;
                              } catch (JSONException e) {
                                 e.printStackTrace();
                                 listener.onError(e.getMessage());
                                 logOutFacebook();
                              }
                              
                              Call<User2> user2Call = myGovServices.loginFacebook("FACEBOOK",email,facebookId);

                              user2Call.enqueue(new Callback<User2>() {
                                    @Override
                                    public void onResponse (Call<User2> call, Response<User2> response) {
                                       if(response.isSuccessful()){
                                          sharedPrefs.saveSharedSetting("email",email);
                                          sharedPrefs.saveSharedSetting("typeLogged","FACEBOOK");
                                          sharedPrefs.saveSharedSetting("passwd",facebookId);
                                             sharedPrefs.saveSharedSetting("logged", true);
                                             sharedPrefs.saveSharedSetting("token", response.body().data.token);
                                          myGovServices.getUsuario(sharedPrefs.readSharedSetting("token","")).enqueue(new Callback<ResponseUsuario>() {
                                             @Override
                                             public void onResponse (Call<ResponseUsuario> call, Response<ResponseUsuario> response) {
                                                sharedPrefs.saveSharedSetting("userId",response.body().data.usuario.id);
                                                listener.onSuccess();
                                             }
      
                                             @Override
                                             public void onFailure (Call<ResponseUsuario> call, Throwable throwable) {
         
                                             }
                                          });
                                       }else {
                                          
                                          Gson gson = new Gson();
                                          Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
                                          if(error.error.code == 401){

                                                Call<User2> signinFacebook = myGovServices.signinFacebook(name, last_name, email, facebookId);
                                                signinFacebook.enqueue(new Callback<User2>() {
                                                   @Override
                                                   public void onResponse (Call<User2> call, Response<User2> response) {
                                                      if(response.isSuccessful()){
                                                            sharedPrefs.saveSharedSetting("email",email);
                                                         sharedPrefs.saveSharedSetting("typeLogged","FACEBOOK");
                                                         sharedPrefs.saveSharedSetting("passwd",facebookId);
                                                            sharedPrefs.saveSharedSetting("logged", true);
                                                            sharedPrefs.saveSharedSetting("token", response.body().data.token);
                                                         myGovServices.getUsuario(sharedPrefs.readSharedSetting("token","")).enqueue(new Callback<ResponseUsuario>() {
                                                            @Override
                                                            public void onResponse (Call<ResponseUsuario> call, Response<ResponseUsuario> response) {
                                                               sharedPrefs.saveSharedSetting("userId",response.body().data.usuario.id);
                                                               listener.onSuccess();
                                                            }
      
                                                            @Override
                                                            public void onFailure (Call<ResponseUsuario> call, Throwable throwable) {
         
                                                            }
                                                         });
                                                      } else {
                                                         Gson gson = new Gson();
                                                         Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
                                                         listener.onError(error.error.message);
                                                         logOutFacebook();
                                                      }
                                                   }
   
                                                   @Override
                                                   public void onFailure (Call<User2> call, Throwable throwable) {
                                                      listener.onError(throwable.getMessage());
                                                      logOutFacebook();
                                                   }
                                                });

                                          }
                                          listener.onError(error.error.message);
                                          logOutFacebook();
                                       }
                                    }
                                    @Override
                                    public void onFailure (Call<User2> call, Throwable throwable) {
                                       listener.onError(throwable.getMessage());
                                       logOutFacebook();
                                    }
                                 });
                              
                           }
                        });
                  Bundle parameters = new Bundle();
                  parameters.putString("fields", "id,first_name,last_name,email");
                  request.setParameters(parameters);
                  request.executeAsync();

               }
            
               @Override
               public void onCancel() {
                  Utils.showToast(context,"Inicio de sesión cancelado");
               }
            
               @Override
               public void onError(FacebookException exception) {
                  Utils.showToast(context,"Error al iniciar sesión");
                  listener.onError(exception.getMessage());
               }
            });
      LoginManager.getInstance().logInWithReadPermissions(context, Arrays.asList("public_profile","email"));
   }
   
   
   public void performLoginGoogle(GoogleSignInClient mGoogleSigninClient){
      
      Intent signInIntent = mGoogleSigninClient.getSignInIntent();
      context.startActivityForResult(signInIntent, RC_SIGN_IN);
   }
   public void performRegisterLoginApiGoogle(final String nombre, final String apellido, final String email, final String token_google){
      myGovServices = initRetrofit(context);
      sharedPrefs = new SharedPrefs(context);
      Call<User2> user2Call = myGovServices.signinGoogle(nombre,apellido,email,token_google);
      user2Call.enqueue(new Callback<User2>() {
         @Override
         public void onResponse (Call<User2> call, Response<User2> response) {
            if(response.isSuccessful()){
               sharedPrefs.saveSharedSetting("email",email);
               sharedPrefs.saveSharedSetting("nombre",nombre);
               sharedPrefs.saveSharedSetting("apellido",apellido);
               sharedPrefs.saveSharedSetting("token_google",token_google);
               sharedPrefs.saveSharedSetting("passwd",token_google);
               sharedPrefs.saveSharedSetting("typeLogged","GOOGLE");
               sharedPrefs.saveSharedSetting("logged", true);
               sharedPrefs.saveSharedSetting("token", response.body().data.token);
               myGovServices.getUsuario(sharedPrefs.readSharedSetting("token","")).enqueue(new Callback<ResponseUsuario>() {
                  @Override
                  public void onResponse (Call<ResponseUsuario> call, Response<ResponseUsuario> response) {
                     sharedPrefs.saveSharedSetting("userId",response.body().data.usuario.id);
                     listener.onSuccess();
                  }
      
                  @Override
                  public void onFailure (Call<ResponseUsuario> call, Throwable throwable) {
         
                  }
               });
            } else {
               Gson gson = new Gson();
               Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
               listener.onError(error.error.message);
               logOutFacebook();
            }
         }
         
         @Override
         public void onFailure (Call<User2> call, Throwable throwable) {
            listener.onError(throwable.getMessage());
         }
      });
   }
   
   private void logOutFacebook(){
      LoginManager.getInstance().logOut();
      AccessToken.setCurrentAccessToken(null);
   }
   
   
}
