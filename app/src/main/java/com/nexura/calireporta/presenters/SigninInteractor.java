package com.nexura.calireporta.presenters;

import android.content.Context;

import com.facebook.CallbackManager;
import com.google.gson.Gson;
import com.nexura.calireporta.api.MyGovServices;
import com.nexura.calireporta.models.Error;
import com.nexura.calireporta.models.ResponsenuevoUsuario2;
import com.nexura.calireporta.views.CompleteListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.nexura.calireporta.utils.Utils.initRetrofit;

public class SigninInteractor {
   CompleteListener listener;
   MyGovServices myGovServices;
   Context context;
   CallbackManager callbackManager;
   SigninInteractor(CompleteListener listener){
      this.listener = listener;
      this.callbackManager = callbackManager = CallbackManager.Factory.create();
   }
   
   public void performSignin(final String name, final String lastName, final String email, final String password){
      myGovServices = initRetrofit(context);
      Call<ResponsenuevoUsuario2> responsenuevoUsuarioCall = myGovServices.nuevoUsuario(name,lastName,email,password);
      responsenuevoUsuarioCall.enqueue(new Callback<ResponsenuevoUsuario2>() {
         @Override
         public void onResponse (Call<ResponsenuevoUsuario2> call, Response<ResponsenuevoUsuario2> response) {
            if (response.isSuccessful()){
               if(response.body().data.success){
                  listener.onSuccess(response.body().data.message);
               } else {
                  listener.onError(response.body().data.message);
               }
            } else {
               Gson gson = new Gson();
               Error error = gson.fromJson(response.errorBody().charStream(),Error.class);
               listener.onError(error.error.message);
            }
         }
   
         @Override
         public void onFailure (Call<ResponsenuevoUsuario2> call, Throwable throwable) {
            listener.onError(throwable.getMessage());
         }
      });
   }
}
