package com.nexura.calireporta.presenters;

import android.content.Context;

import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.CompleteListener;
import com.nexura.calireporta.views.ForgotPasswordView;

public class ForgotPasswordPresenter implements CompleteListener {
   ForgotPasswordView view;
   ForgotPasswordInteractor interactor;
   
   public ForgotPasswordPresenter(ForgotPasswordView view, Context context){
      this.view = view;
      this.interactor = new ForgotPasswordInteractor(this);
      this.interactor.context = context;
   }
   
   public void attemptSendEmail(String email){
      if(view != null){
         view.showProgress();
         view.setUsernameError("");
         if(email.isEmpty()){
            view.setUsernameError("Correo es requerido");
            view.hideProgress();
            return;
         }
         if(!Utils.isValidEmail(email)){
            view.setUsernameError("Ingresa un email valido");
            view.hideProgress();
            return;
         }
         interactor.performSendEmail(email);
      }
   }
   @Override
   public void onSuccess () {
   
   }
   
   @Override
   public void onSuccess (String message) {
      if(view != null){
         view.hideProgress();
         view.onSendEmailSuccess(message);
      }
   }
   
   @Override
   public void onError (String error) {
      if(view != null){
         view.hideProgress();
         view.setSendEmailError(error);
      }
   }
   
   public void onDestroy(){
      view = null;
   }
   
}
