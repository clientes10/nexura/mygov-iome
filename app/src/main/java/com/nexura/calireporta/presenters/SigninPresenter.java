package com.nexura.calireporta.presenters;

import android.content.Context;

import com.nexura.calireporta.utils.Utils;
import com.nexura.calireporta.views.CompleteListener;
import com.nexura.calireporta.views.SigninView;

public class SigninPresenter implements CompleteListener {
   SigninView view;
   SigninInteractor interactor;
   
   public SigninPresenter(SigninView view, Context context){
      this.view = view;
      this.interactor = new SigninInteractor(this);
      this.interactor.context = context;
   }
   
   public void attemptSigin(String name, String lastName, String email, String password){
      if(view != null) {
         view.showProgress();
         view.setNameError("");
         view.setPasswordError("");
         view.setLastNameError("");
         view.setUsernameError("");
         if (name.isEmpty()) {
            view.setNameError("Nombre es requerido");
            view.hideProgress();
            return;
         }
         if (lastName.isEmpty()) {
            view.setLastNameError("Apellido es requerido");
            view.hideProgress();
            return;
         }
         if (email.isEmpty()) {
            view.setUsernameError("Email es requerido");
            view.hideProgress();
            return;
         }
         if (!Utils.isValidEmail(email)) {
            view.setUsernameError("Ingrese un correo válido");
            view.hideProgress();
            return;
         }
         if (password.isEmpty()) {
            view.setPasswordError("Campo password es requerido");
            view.hideProgress();
            return;
         }
         interactor.performSignin(name,lastName,email,password);
      }
   }
   
   @Override
   public void onSuccess () {
   
   }
   
   @Override
   public void onSuccess (String message) {
      if(view != null){
         view.hideProgress();
         view.onSigninSuccess(message);
      }
   }
   
   @Override
   public void onError (String error) {
      if(view != null){
         view.hideProgress();
         view.setSigninError(error);
      }
   }
   
   public void onDestroy(){
      view = null;
   }
   
   
}
