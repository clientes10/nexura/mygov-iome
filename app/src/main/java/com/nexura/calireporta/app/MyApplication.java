package com.nexura.calireporta.app;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.nexura.calireporta.models.Categoria;
import com.nexura.calireporta.models.Comuna;
import com.nexura.calireporta.models.Estado;
import com.nexura.calireporta.models.Evento;
import com.nexura.calireporta.models.EventoSingle;
import com.nexura.calireporta.models.MapConfig;
import com.nexura.calireporta.models.ResponseEventos2;
import com.nexura.calireporta.models.SubCategoria;
import com.nexura.calireporta.models.TipoValoracion;
import com.nexura.calireporta.models.User;

import java.util.List;

/**
 * Created by javpoblano on 26/03/17.
 */

public class MyApplication extends Application {

    public static MyApplication sInstance;
    public static List<EventoSingle> eventos = null;
    public static List<EventoSingle> misEventos = null;
    public static List<EventoSingle> eventosBackup = null;
    public static List<EventoSingle> misEventosBackup = null;
    public static List<EventoSingle> eventosMeAfectaBackup = null;
    public static List<Evento> filteredEvents = null;
    public static List<Comuna> comunas = null;
    public static List<Categoria> categorias = null;
    public static List<SubCategoria> subcategorias = null;
    public static List<Estado> estados = null;
    public static List<TipoValoracion> valoraciones = null;
    public static User user = null;
    public static ResponseEventos2 responseEventos = null;
    public static MapConfig mapConfig = null;
    
    @Override
    public void onCreate () {
            super.onCreate();
            //FacebookSdk.fullyInitialize();
            AppEventsLogger.activateApp(this);
            sInstance=this;
    }
    
    public static List<EventoSingle> getMisEventosBackup() {
        return misEventosBackup;
    }

    public static void setMisEventosBackup(List<EventoSingle> misEventosBackup) {
        MyApplication.misEventosBackup = misEventosBackup;
    }

    public static List<EventoSingle> getEventosMeAfectaBackup() {
        return eventosMeAfectaBackup;
    }

    public static void setEventosMeAfectaBackup(List<EventoSingle> eventosMeAfectaBackup) {
        MyApplication.eventosMeAfectaBackup = eventosMeAfectaBackup;
    }
    
    public static List<EventoSingle> getMisEventos() {
        return misEventos;
    }

    public static void setMisEventos(List<EventoSingle> misEventos) {
        MyApplication.misEventos = misEventos;
    }

    public static List<EventoSingle> getEventosBackup() {
        return eventosBackup;
    }

    public static void setEventosBackup(List<EventoSingle> eventosBackup) {
        MyApplication.eventosBackup = eventosBackup;
    }

    public static MyApplication getInstance() {
        return sInstance;
    }

    public static List<EventoSingle> getEventos() {
        return eventos;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        MyApplication.user = user;
    }

    public static void setEventos(List<EventoSingle> evento) {
        eventos = evento;
    }

    public static List<Evento> getFilteredEvents() {
        return filteredEvents;
    }

    public static void setFilteredEvents(List<Evento> filteredEvent) {
        filteredEvents = filteredEvent;
    }



    public static List<Categoria> getCategorias() {
        return categorias;
    }

    public static void setCategorias(List<Categoria> categorias2) {
        categorias = categorias2;
    }

    public static List<SubCategoria> getSubcategorias() {
        return subcategorias;
    }

    public static void setSubcategorias(List<SubCategoria> subcategorias2) {
        subcategorias = subcategorias2;
    }

    public static List<Estado> getEstados() {
        return estados;
    }

    public static void setEstados(List<Estado> estados) {
        MyApplication.estados = estados;
    }

    public static List<TipoValoracion> getValoraciones() {
        return valoraciones;
    }

    public static void setValoraciones(List<TipoValoracion> valoraciones) {
        MyApplication.valoraciones = valoraciones;
    }

    public static MyApplication getsInstance() {
        return sInstance;
    }

    public static void setsInstance(MyApplication sInstance) {
        MyApplication.sInstance = sInstance;
    }

    public static List<Comuna> getComunas() {
        return comunas;
    }

    public static void setComunas(List<Comuna> comunas2) {
        comunas = comunas2;
    }

    public static ResponseEventos2 getResponseEventos() {
        return responseEventos;
    }

    public static void setResponseEventos(ResponseEventos2 responseEventos) {
        MyApplication.responseEventos = responseEventos;
    }

    public static int getPosicionMisEventosconId(int id){
        int position = 0;
        for(int i = 0; i<misEventos.size();i++){
            if(misEventos.get(i).id == id)
                position = i;
        }
        return position;
    }

    public static Categoria getCategoriasconId(int id){
        Categoria categoria = null;
        for(int i = 0; i<categorias.size();i++){
            if(categorias.get(i).id == id)
                categoria = categorias.get(i);
        }
        return categoria;
    }
    
    public static MapConfig getMapConfig () {
        return mapConfig;
    }
    
    public static void setMapConfig (MapConfig mapConfig) {
        MyApplication.mapConfig = mapConfig;
    }
}
