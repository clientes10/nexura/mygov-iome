package com.nexura.calireporta.interfaces;

/**
 * Created by javpoblano on 20/03/17.
 */

public interface MainActivityUIEvents {
    void onTabSelected(int position);
}
